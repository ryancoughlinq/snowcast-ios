//
//  SNWTrackerDetailView.h
//  Pods
//
//  Created by Ryan Coughlin on 12/21/14.
//
//

#import <UIKit/UIKit.h>

@interface SNWTrackerItemView : UIView

@property (weak, nonatomic) IBOutlet UILabel *itemHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemLabelValue;
@property (weak, nonatomic) IBOutlet UIView *itemView;

@end
