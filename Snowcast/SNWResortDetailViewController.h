@import CoreLocation;
#import "SNWResortPreview.h"
#import "SNWResortPieView.h"

@interface SNWResortDetailViewController : UIViewController

@property (strong, nonatomic) SNWResortPreview *resort;
@property (nonatomic) NSNumber *resortID;
@property (strong, nonatomic) CLLocation *userLocation;
@property CLLocation *resortLocation;

@property (weak, nonatomic) IBOutlet UIScrollView *weatherView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *resortNameLabel;
@property (weak, nonatomic) IBOutlet SNWResortPieView *openPercentPieView;
@property (weak, nonatomic) IBOutlet SNWResortPieView *trailStatusPieView;
@property (weak, nonatomic) IBOutlet SNWResortPieView *liftStatusPieView;

@end
