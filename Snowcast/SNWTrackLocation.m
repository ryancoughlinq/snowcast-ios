@import CoreLocation;
#import "SNWTrackLocation.h"

@interface SNWTrackLocation () <CLLocationManagerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSDate *locationManagerStartedDate;

@end

@implementation SNWTrackLocation

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.activityType = CLActivityTypeFitness;

    return self;
}

- (void)beginTrackingUserMovements {
    [self.locationManager startUpdatingLocation];
}

- (void)stopTrackingUserMovements {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = locations.lastObject;
    NSDate *lastLocationDate = location.timestamp;
    NSDate *fiveSecondsAgo = [NSDate dateWithTimeIntervalSinceNow:-5];
    NSComparisonResult result = [lastLocationDate compare:fiveSecondsAgo];

    if (location.horizontalAccuracy > 0 || !location || result != NSOrderedAscending) {
        self.currentLocation = locations.lastObject;
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
    } else if (status != kCLAuthorizationStatusNotDetermined) {
        NSLog(@"Authorzied status not determined");
    }
}

@end
