#import "SNWResortPreview.h"

@implementation SNWResortPreview : MTLModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name",
             @"resortID": @"id",
             @"type": @"type"
             };
}

@end
