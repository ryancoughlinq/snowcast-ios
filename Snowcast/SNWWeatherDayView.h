//
//  SNWWeatherDayView.h
//  Snowcast
//
//  Created by Ryan Coughlin on 9/28/14.
//  Copyright (c) 2014 Ryan Coughlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNWWeatherDayView : UIView

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherIcon;
@property (weak, nonatomic) IBOutlet UILabel *temperatureHighLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLowLabel;

- (instancetype)initWithFrame:(CGRect)frame precipitationWidth:(CGFloat)width precipType:(NSString *)type;
@end
