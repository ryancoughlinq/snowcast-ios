@interface SNWCircularProgressView : UIView

@property (nonatomic) CGFloat progress;
@property (nonatomic) UIColor *progressColor;
@property (nonatomic) IBInspectable CGFloat progressWidth;

@end
