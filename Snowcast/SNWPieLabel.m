#import "SNWPieLabel.h"

@implementation SNWPieLabel

- (void)drawTextInRect:(CGRect)rect
{
    UIEdgeInsets insets = {0, 2, 0, 2};
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}
@end
