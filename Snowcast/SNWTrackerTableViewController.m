#import "SNWRunData.h"
#import "SNWTrackerRunTableViewCell.h"
#import "SNWTrackerViewController.h"
#import "SNWTrackerTableViewController.h"
#import "SNWTrackerViewModel.h"
#import "SNWTheme.h"

static NSString *kCellIdentifier = @"Cell";

@interface SNWTrackerTableViewController ()

@property (nonatomic, strong) RLMNotificationToken *notification;
@property (nonatomic, strong) SNWTheme *theme;
@property (nonatomic, strong) RLMResults *runs;

@end

@implementation SNWTrackerTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self styleTableView];

//    self.runs = [SNWRunData allObjects];
//    [self.tableView reloadData];

//    __weak typeof(self) weakSelf = self;
//    self.notification = [RLMRealm.defaultRealm addNotificationBlock:^(NSString *note, RLMRealm *realm) {
//        [weakSelf.tableView reloadData];
//    }];
}

- (void)styleTableView
{
    self.theme = [SNWTheme themeFromPlistNamed:@"Styles"];
    self.tableView.separatorColor = self.theme.slate;
    self.tableView.backgroundColor = self.theme.baseBackground;
    self.automaticallyAdjustsScrollViewInsets = NO;

    UINib *myNib = [UINib nibWithNibName:@"SNWTrackerRunTableCellNib" bundle:nil];
    [self.tableView registerNib:myNib forCellReuseIdentifier:kCellIdentifier];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (NSInteger)self.runs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNWTrackerRunTableViewCell *cell = (SNWTrackerRunTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];

    SNWRunData *run = [self.runs objectAtIndex:(NSUInteger)indexPath.row];
    SNWTrackerViewModel *viewModel = [[SNWTrackerViewModel alloc] initWithRunData:run];

    cell.distanceLabel.text = viewModel.distance;
    cell.maxSpeed.text = viewModel.maxSpeed;
    cell.runDateLabel.text = viewModel.formattedDateString;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        RLMRealm *realm = RLMRealm.defaultRealm;
        [realm beginWriteTransaction];
        [realm deleteObject:self.runs[(NSUInteger)indexPath.row]];
        [realm commitWriteTransaction];
        self.runs = [SNWRunData allObjects];
        [tableView endUpdates];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Navigation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"showDetailRun" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if([segue.identifier isEqualToString:@"showDetailRun"]) {
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        SNWTrackerViewController *trackerVC = segue.destinationViewController;
//        trackerVC.trackRun = [self.runs objectAtIndex:indexPath.row];
//    }
}

@end
