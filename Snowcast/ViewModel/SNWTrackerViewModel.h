//
//  SNWTrackerViewModel.h
//  Snowcast
//
//  Created by Ryan Coughlin on 1/12/15.
//  Copyright (c) 2015 Ryan Coughlin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNWRunData.h"

@interface SNWTrackerViewModel : NSObject

- (instancetype)initWithRunData:(SNWRunData *)runData;

@property (nonatomic, readonly) NSString *maxSpeed;
@property (nonatomic, readonly) NSString *maxAltitude;
@property (nonatomic, readonly) NSString *altitudeDelta;
@property (nonatomic, readonly) NSString *averageSpeed;
@property (nonatomic, readonly) NSString *distance;
@property (nonatomic, readonly) NSString *formattedDateString;
@property (nonatomic) NSDate *date;

@end
