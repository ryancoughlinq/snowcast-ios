#import "SNWTrackerViewModel.h"
#import "SNWRunData.h"

const double kMpsConversion = 2.236936284;

@interface SNWTrackerViewModel ()

@property (nonatomic) SNWRunData *trackerRun;

@end

@implementation SNWTrackerViewModel

- (instancetype)initWithRunData:(SNWRunData *)runData
{
    self = [super init];
    if (!self) return nil;

    self.trackerRun = runData;
    self.date = self.trackerRun.date;

    return self;
}

- (NSString *)maxSpeed
{
    return [NSString stringWithFormat:@"%@ mph", [self formatNumberWithDouble:self.trackerRun.maxSpeed]];
}

- (NSString *)averageSpeed
{
    return [NSString stringWithFormat:@"%@ mph", [self formatNumberWithDouble:self.trackerRun.averageSpeed]];
}

- (NSString *)maxAltitude
{
    return [NSString stringWithFormat:@"%f", self.trackerRun.maxAltitude];
}

- (NSString *)distance
{
    NSLengthFormatter *lengthFormatter = [NSLengthFormatter new];
    NSString *formattedDistance = [self formatNumberWithDouble:self.trackerRun.distance];
    return [lengthFormatter stringFromMeters:[formattedDistance doubleValue]];
}

- (NSString *)formatNumberWithDouble:(double)value
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.maximumFractionDigits = 0;
    return [formatter stringFromNumber:[NSNumber numberWithDouble:value]];
}

- (NSNumber *)convertMPSToMPHWithNumber:(NSNumber *)mpsNumber
{
    return [NSNumber numberWithDouble:[mpsNumber doubleValue] * kMpsConversion];
}

- (NSString *)formattedDateString
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"EEEE, MMM dd h:mma"];
    return [formatter stringFromDate:self.date];
}
@end
