@import QuartzCore;
#import "SNWResort.h"
#import "SNWResortDetailViewController.h"
#import "SNWService.h"
#import "SNWGetLocation.h"
#import "SNWWeatherDayView.h"
#import "SNWWeather.h"
#import <BlocksKit.h>
#import "SNWTheme.h"

static const float kWeatherDayHeight = 178;
static const float kWeatherDayWidth = 80;

@interface SNWResortDetailViewController ()

@property (nonatomic, strong) SNWTheme *theme;
@property (nonatomic, strong) NSArray *weatherForecast;
@property (nonatomic, strong) SNWWeatherDayView *weatherDayView;

@end

@implementation SNWResortDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    SNWService *snowService = [SNWService new];
    self.theme = [SNWTheme themeFromPlistNamed:@"Styles"];
    self.view.backgroundColor = self.theme.baseBackground;
    self.resortNameLabel.font = [self.theme fontWithNameKey:@"secondaryFont" sizeKey:@"headlineFontSize"];

    [[snowService fetchResortWithID:self.resort.resortID] subscribeNext:^(SNWResort *singleResort) {
        
        self.resortNameLabel.text = singleResort.name;
        self.openPercentPieView.pieLabel.text = singleResort.openDownHillPercent;
        self.openPercentPieView.pieValueLabel.text = singleResort.openDownHillPercent;
        self.openPercentPieView.pieValueLabel.textColor = self.theme.green;
        self.openPercentPieView.progress = 0.0;
        
        self.resortLocation = singleResort.resortLocation;
    }];
    
    [[[[RACObserve(self, resortLocation)
        ignore: nil]
        deliverOn:RACScheduler.mainThreadScheduler]
        flattenMap:^(CLLocation *location) {
           return [snowService fetchWeatherWithLocation:location.coordinate];
        }]
        subscribeNext:^(NSArray *daysOfWeather) {
            self.weatherForecast = daysOfWeather;
        }];
    
    [[[RACObserve(self, weatherForecast)
     ignore:nil]
    deliverOn:RACScheduler.mainThreadScheduler]
     subscribeNext:^(NSArray *forecast) {
         __block int scrollWidth = 0;
         __block UIColor *todayColorBackground = self.theme.slate;

         [forecast enumerateObjectsUsingBlock:^(SNWWeather *day, NSUInteger idx, BOOL *stop) {
             self.weatherDayView = [[[NSBundle mainBundle] loadNibNamed:@"SNWWeatherDayView" owner:self options:nil] objectAtIndex:0];

             float precipitationWidth = (kWeatherDayWidth * [day.precipProbability floatValue]);

             self.weatherDayView = [[SNWWeatherDayView alloc] initWithFrame:CGRectMake(scrollWidth, 0, kWeatherDayWidth, kWeatherDayHeight)
                                                         precipitationWidth:precipitationWidth
                                                                 precipType:day.precipType];

             if(idx == 0) {
                 self.weatherDayView.backgroundColor = todayColorBackground;
             }

             if(idx != 0) {
                 // Build vertical border
                 CALayer *bottomBorder = [CALayer layer];
                 bottomBorder.frame = CGRectMake(0, 0, 0.50f, kWeatherDayHeight);
                 bottomBorder.backgroundColor = [self.theme.slate CGColor];
                 [self.weatherDayView.layer addSublayer:bottomBorder];
             }

             self.weatherDayView.weatherIcon.image = [UIImage imageNamed:day.icon];
             self.weatherDayView.temperatureHighLabel.text = [NSString stringWithFormat:@"%@°", [self trimDecimalPlacesWithNumber:day.temperatureMax]];
             self.weatherDayView.temperatureLowLabel.text = [NSString stringWithFormat:@"%@°", [self trimDecimalPlacesWithNumber:day.temperatureMin]];
             self.weatherDayView.dayLabel.text = [[self formatDateToWeekdayWithDate:day.date] uppercaseString];

             [self.contentView addSubview:self.weatherDayView];

             scrollWidth = scrollWidth + kWeatherDayWidth;
         }];

         self.weatherView.backgroundColor = [UIColor redColor];
         self.weatherView.showsHorizontalScrollIndicator = NO;

         [self.weatherView setContentSize:CGSizeMake(scrollWidth, kWeatherDayHeight)];

         // Build top weather border
         CALayer *topBorder = [CALayer layer];
         topBorder.frame = CGRectMake(0, 0, self.weatherView.contentSize.width, 0.50f);
         topBorder.backgroundColor = [self.theme.slate CGColor];
         [self.weatherView.layer addSublayer:topBorder];
     }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.openPercentPieView.progress = 0.65;
    self.trailStatusPieView.progress = 0.95;
    self.liftStatusPieView.progress = 0.25;
}
     
- (NSString *)formatDateToWeekdayWithDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EE"];

    return [dateFormatter stringFromDate:date];
}

- (NSString *)trimDecimalPlacesWithNumber:(NSNumber *)number
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];

    return [formatter stringFromNumber:number];
}


@end
