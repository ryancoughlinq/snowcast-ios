@import CoreLocation;

@interface SNWResort : NSObject

- (instancetype)initWithResortDictionary:(NSDictionary *)resort;

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *openTrails;
@property (nonatomic, readonly) NSString *totalTrails;
@property (nonatomic, readonly) NSString *openLifts;
@property (nonatomic, readonly) NSString *totalLifts;
@property (nonatomic, readonly) NSString *totalSnow;
@property (nonatomic, readonly) NSString *openDownHillPercent;
@property CLLocation *resortLocation;

@end
