#import <Mantle.h>

@interface SNWResortPreview : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *resortID;
@property (nonatomic, strong) NSString *type;

@end
