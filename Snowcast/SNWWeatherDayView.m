//
//  SNWWeatherDayView.m
//  Snowcast
//
//  Created by Ryan Coughlin on 9/28/14.
//  Copyright (c) 2014 Ryan Coughlin. All rights reserved.
//

@import QuartzCore;
#import "SNWWeatherDayView.h"
#import "SNWTheme.h"

@interface SNWWeatherDayView ()

@property (nonatomic, strong) SNWTheme *theme;
@property CGFloat width;

@end

@implementation SNWWeatherDayView

- (instancetype)initWithFrame:(CGRect)frame precipitationWidth:(CGFloat)width precipType:(NSString *)type
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"SNWWeatherDayView" owner:self options:nil] objectAtIndex:0];
    self.theme = [SNWTheme themeFromPlistNamed:@"Styles"];

    if (self) {

        self.frame = frame;
        _width = width;
        self.backgroundColor = self.theme.baseBackground;
        self.dayLabel.font = [self.theme fontWithNameKey:@"bodyFont" sizeKey:@"smallLabel"];
        self.dayLabel.textColor = [UIColor whiteColor];
        self.temperatureHighLabel.textColor = [UIColor whiteColor];
        self.temperatureHighLabel.font = [self.theme fontWithNameKey:@"secondaryFont" sizeKey:@"headlineFontSize"];
        self.temperatureLowLabel.font = [self.theme fontWithNameKey:@"secondaryFont" sizeKey:@"headlineFontSize"];
        self.temperatureLowLabel.textColor = self.theme.lightGrey;

        [self setupGradientsWithType:type];
    }

    return self;
}

 - (void)setupGradientsWithType:(NSString *)gradientType
{
    if(gradientType) {
        CGFloat height = 6;
        CGFloat bottomY = self.frame.size.height - height;

        CAGradientLayer *dayGradientType;

        if([gradientType isEqualToString:@"snow"]) {
            dayGradientType = [self buildTwoStopHorziontalGradientWithColors:self.theme.iceGreen ColorTwo:self.theme.mainBlue];
        } else {
            dayGradientType = [self buildTwoStopHorziontalGradientWithColors:self.theme.lightGreen ColorTwo:self.theme.green];
        }

        dayGradientType.frame = CGRectMake(0, bottomY, self.width, height);

        [self.layer insertSublayer:dayGradientType atIndex:0];
    }
}

- (CAGradientLayer *)buildTwoStopHorziontalGradientWithColors:(UIColor *)colorOne ColorTwo:(UIColor *)colorTwo
{
    UIColor *colorStopOne = colorOne;
    UIColor *colorStopTwo = colorTwo;
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorStopOne.CGColor, colorStopTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.startPoint = CGPointMake(0, 0.5);
    headerLayer.endPoint = CGPointMake(1.0, 0.5);
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    
    return headerLayer;
}

@end
