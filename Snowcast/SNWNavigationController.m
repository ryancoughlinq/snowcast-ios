//
//  SNWNavigationController.m
//  Snowcast
//
//  Created by Ryan Coughlin on 11/4/14.
//  Copyright (c) 2014 Ryan Coughlin. All rights reserved.
//

#import "SNWNavigationController.h"
#import "SNWTheme.h"

@interface SNWNavigationController ()

@property (nonatomic, strong) SNWTheme *theme;

@end

@implementation SNWNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationBar.backgroundColor = self.theme.baseBackground;
    self.navigationBar.barTintColor = self.theme.baseBackground;
    [self.navigationBar setTranslucent:NO];
    [self.navigationBar setShadowImage:nil];
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]]];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:nil
                                                                            action:nil];
    self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"back-arrow"];

}
@end
