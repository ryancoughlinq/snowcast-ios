@import MapKit;

@interface SNWTrackerMapView : MKMapView <MKMapViewDelegate>

- (void)drawPolylineWithLocationArray:(RLMArray *)array;
- (void)zoomToPolyLine:(MKMapView *)map
              polyLine:(MKPolyline *)polyLine
              animated:(BOOL)animated;

@end
