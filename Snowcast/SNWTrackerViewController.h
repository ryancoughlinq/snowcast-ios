@import MapKit;
#import "SNWRunData.h"

@interface SNWTrackerViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, strong) SNWRunData *trackRun;

@end
