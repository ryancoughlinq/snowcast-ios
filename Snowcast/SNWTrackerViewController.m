#import <Realm.h>
#import "SNWTrackerViewModel.h"
#import "SNWTrackerViewController.h"
#import "SNWTrackerMapView.h"
#import "SNWTrackLocation.h"
#import "SNWTheme.h"
#import "SNWTrackerItemView.h"
#import "SNWTrackerController.h"

@interface SNWTrackerViewController ()

@property CGFloat viewWidth;
@property CGFloat viewHeight;
@property (nonatomic, strong) SNWTheme *theme;
@property (nonatomic, strong) SNWTrackerController *trackerController;
@property (nonatomic, strong) SNWTrackLocation *trackerLocation;
@property (nonatomic, strong) SNWTrackerMapView *mapView;
@property (nonatomic, strong) SNWTrackerItemView *itemView;
@property (nonatomic, readwrite) SNWTrackerViewModel *trackerViewModel;
@property (weak, nonatomic) IBOutlet SNWTrackerItemView *maxAltitudeView;
@property (weak, nonatomic) IBOutlet SNWTrackerItemView *elevationChangeView;
@property (weak, nonatomic) IBOutlet SNWTrackerItemView *totalDistanceView;
@property (weak, nonatomic) IBOutlet SNWTrackerItemView *maxSpeedView;
@property (weak, nonatomic) IBOutlet SNWTrackerItemView *averageSpeedView;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *timer;
@property (weak, nonatomic) IBOutlet UIButton *finishRunButton;
@property (nonatomic, strong) RACSignal *startTracker;
@property (nonatomic, strong) RACSignal *pauseTracker;
@property (nonatomic, strong) RACSignal *finishTracker;
@end

@implementation SNWTrackerViewController

@synthesize segmentedControl;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.viewWidth = self.view.bounds.size.width;
    self.viewHeight = self.view.bounds.size.height;

    self.mapView = [[SNWTrackerMapView alloc] initWithFrame:CGRectMake(self.viewWidth, 0, self.viewWidth, self.viewHeight)];
    [self.scrollView addSubview:self.mapView];
    
    self.trackerController = [SNWTrackerController new];
    
    [self styleUI];

    self.startTracker = [self.startButton rac_signalForControlEvents: UIControlEventTouchUpInside];
    self.pauseTracker = [self.pauseButton rac_signalForControlEvents:UIControlEventTouchUpInside];
    self.finishTracker = [self.finishRunButton rac_signalForControlEvents:UIControlEventTouchUpInside];

    [[RACObserve(self, trackerViewModel) deliverOnMainThread] subscribeNext:^(SNWTrackerViewModel *trackerModel) {
        self.totalDistanceView.itemLabelValue.text = trackerModel.distance;
        self.maxAltitudeView.itemLabelValue.text = trackerModel.maxAltitude;
        self.elevationChangeView.itemLabelValue.text = trackerModel.altitudeDelta;
        self.maxSpeedView.itemLabelValue.text = trackerModel.maxSpeed;
        self.averageSpeedView.itemLabelValue.text = trackerModel.averageSpeed;
    }];

    [[self.startTracker flattenMap:^id(UIButton *startButton) {
        [startButton setHidden:YES];
        return [self.trackerController timerWithPauseButton:self.pauseTracker];
    }] subscribeNext:^(NSString *timerLabel)  {
        self.timer.text = timerLabel;
    }];

    [self.pauseTracker subscribeNext:^(id x) {
        [self.startButton setHidden:NO];
    }];

    [[[self.finishTracker flattenMap:^id(id x) {
        return [self.trackerController buildRun];
    }]
    flattenMap:^id(SNWRunData *runData) {
        [self.mapView drawPolylineWithLocationArray:runData.locationUpdates];
        return [self.trackerController createViewModelWithRunData:runData];
    }]
    subscribeNext:^(SNWTrackerViewModel *trackerViewModel) {
        self.trackerViewModel = trackerViewModel;
    }];
}

- (void)viewDidLayoutSubviews
{
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width * 2, self.view.frame.size.height)];
}

- (IBAction)closeTrackerModal:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)styleUI
{
    self.theme = [SNWTheme themeFromPlistNamed:@"Styles"];
    self.view.backgroundColor = self.theme.baseBackground;
    self.totalDistanceView.itemHeaderLabel.text = @"TOTAL DISTANCE";
    self.elevationChangeView.itemHeaderLabel.text = @"ELEVATION CHANGE";
    self.maxAltitudeView.itemHeaderLabel.text = @"MAX ALTITUDE";
    self.maxSpeedView.itemHeaderLabel.text = @"MAX SPEED";
    self.segmentedControl.frame = CGRectMake(0, 0, 150, 26);
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    double pageWidth = scrollView.frame.size.width;
    double page = scrollView.contentOffset.x / pageWidth;
    segmentedControl.selectedSegmentIndex = (int)page;
}

- (IBAction)segmentedControl:(id)sender
{
    if (segmentedControl.selectedSegmentIndex == 0) {
        [self.scrollView scrollRectToVisible:CGRectMake(0, 0, self.viewWidth, self.viewHeight) animated:YES];
    } else {
        [self.scrollView scrollRectToVisible:CGRectMake(320, 0, self.viewWidth, self.viewHeight) animated:YES];
    }
}

@end
