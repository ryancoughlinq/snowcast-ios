@import CoreLocation;

@interface SNWGetLocation : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocation *currentLocation;

- (void)findCurrentLocation;

@end
