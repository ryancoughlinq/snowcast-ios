@import CoreLocation;

@interface SNWService : NSObject

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSMutableArray *alpineResorts;
@property (nonatomic, strong) NSMutableArray *nordicResorts;

- (RACSignal *)fetchDataWithURL:(NSURL *)url;
- (RACSignal *)fetchAllResorts;
- (RACSignal *)fetchResortWithID:(NSNumber *)resortID;
- (RACSignal *)fetchResortsNearWithUserLocation:(CLLocationCoordinate2D)coordinate;
- (RACSignal *)fetchWeatherWithLocation:(CLLocationCoordinate2D)coordinate;
- (NSString *)convertCoordinateToString:(CLLocationCoordinate2D)coordinate;
- (NSURL *)createURLWithParameters:(NSString *)resortParameters;
- (NSURL *)createURLWithString:(NSString *)string;

@end
