#import "SNWTrackerViewModel.h"

@interface SNWTrackerController : NSObject

- (RACSignal *)buildRun;
- (RACSignal *)createViewModelWithRunData:(SNWRunData *)runData;
- (RACSignal *)timerWithPauseButton:(RACSignal *)pauseButton;

@end
