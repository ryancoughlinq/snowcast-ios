//
//  SNWResortCell.h
//  
//
//  Created by Ryan Coughlin on 7/29/14.
//
//

#import <UIKit/UIKit.h>

@interface SNWResortCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *resortNameLabel;

@end
