//
//  SNWNavigationController.h
//  Snowcast
//
//  Created by Ryan Coughlin on 11/4/14.
//  Copyright (c) 2014 Ryan Coughlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNWNavigationController : UINavigationController

@end
