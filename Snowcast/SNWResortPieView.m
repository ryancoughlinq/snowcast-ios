#import "SNWTheme.h"
#import <QuartzCore/QuartzCore.h>
#import "SNWResortPieView.h"
#import "SNWCircularProgressView.h"

@interface SNWResortPieView ()

@property (weak, nonatomic) IBOutlet SNWCircularProgressView *progressView;
@property (nonatomic, strong) SNWResortPieView *pieView;
@property (nonatomic, strong) SNWTheme *theme;

@end

@implementation SNWResortPieView

- (id)awakeAfterUsingCoder:(NSCoder *)aDecoder
{
    if (self.subviews.count == 0) {
        self.pieView = [[[NSBundle mainBundle] loadNibNamed:@"SNWResortPieViewNib" owner:nil options:nil] lastObject];
        self.pieView.frame = self.frame;
        self.pieView.autoresizingMask = self.autoresizingMask;
        self.pieView.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.theme = [SNWTheme themeFromPlistNamed:@"Styles"];
        
        [self styleLabel];
    
        return self.pieView;
    }
    
    return self;
}
- (void)styleLabel
{
    self.pieView.pieLabel.layer.backgroundColor = self.theme.baseBackground.CGColor;
    self.pieView.pieLabel.layer.shadowOffset = CGSizeMake(4.0f, 6.0f);
    self.pieView.pieLabel.layer.shadowRadius = 20.0f;
    self.pieView.pieLabel.layer.shadowColor  = [[UIColor blackColor] CGColor];
    self.pieView.pieLabel.layer.shadowOpacity = 1.0f;
    self.pieView.pieLabel.layer.cornerRadius = 8.0f;
}

- (void)setProgress:(CGFloat)progress
{
    self.progressView.progress = progress;
}

- (void)setColor:(UIColor *)color
{
    self.progressView.progressColor = color;
}

- (void)setIsIconHidden:(BOOL)isIconHidden
{
    self.pieImageView.hidden = isIconHidden;
}

- (void)setIsLabelHidden:(BOOL)isLabelHidden
{
    self.pieValueLabel.hidden = isLabelHidden;
}

- (void)setPieImage:(UIImage *)pieImage
{
    self.pieImageView.image = pieImage;
}

@end
