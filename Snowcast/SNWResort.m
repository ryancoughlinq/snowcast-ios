#import "SNWResort.h"

@interface SNWResort ()

@property (nonatomic, readonly) NSDictionary *resort;

@end

@implementation SNWResort

- (instancetype)initWithResortDictionary:(NSDictionary *)resort
{    
    self = [super init];

    if (self == nil)
    {
        return nil;
    }
    
    if (resort == nil)
    {
        NSLog(@"Data is empty");
        return nil;
    }
    
    _resort = resort;
    _name = _resort[@"resortName"];
    _openDownHillPercent = [self formatWithPercentSignFromString:_resort[@"openDownHillPercent"]];
    _status = _resort[@"operatingStatus"];
    _openTrails = [self formatEmptyStringWithZero:_resort[@"openDownHillTrails"]];
    _totalTrails = _resort[@"maxOpenDownHillTrails"];
    _openLifts = [self formatEmptyStringWithZero:_resort[@"openDownHillLifts"]];
    _totalLifts = _resort[@"maxOpenDownHillLifts"];
    _totalSnow = _resort[@"seasonTotal"];
    _resortLocation = [self createLocationWithLatitude:[resort[@"latitude"] doubleValue] longitude:[resort[@"longitude"] doubleValue]];

    return self;
}

- (CLLocation *)createLocationWithLatitude:(double)latitude longitude:(double)longitude
{
    
    CLLocationDegrees latitudeDegrees = latitude;
    CLLocationDegrees longitudeDegrees = longitude;

    CLLocationCoordinate2D locationCoordinates = CLLocationCoordinate2DMake(latitudeDegrees, longitudeDegrees);

    return [[CLLocation alloc] initWithCoordinate:locationCoordinates
                                         altitude:0
                               horizontalAccuracy:0
                                 verticalAccuracy:0
                                           course:0
                                            speed:0
                                        timestamp:[NSDate date]];
}

- (NSString *)formatWithPercentSignFromString:(NSString *)string
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [formatter stringFromNumber:[self convertStringToNumber:string]];
}

- (NSNumber *)convertStringToNumber:(NSString *)string
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [formatter numberFromString:string];
}

- (NSString *)formatEmptyStringWithZero:(NSString *)value
{
    if(value == nil) {
        return @"0";
    }
    
    if ([value isEqualToString:@""]) {
        return @"0";
    }
    return nil;
}

@end
