#import <Mantle.h>

#import "SNWResortDetailViewController.h"
#import "SNWResortCollectionView.h"
#import "SNWService.h"
#import "SNWResortCell.h"
#import "SNWResortPreview.h"
#import "SNWTheme.h"

static NSString * CellIdentifier = @"resortCell";

@interface SNWResortCollectionView () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) SNWTheme *theme;
@property (nonatomic, strong) SNWService *getResorts;
@property (nonatomic, strong) NSArray *resorts;
@property (nonatomic, strong) SNWGetLocation *getUserLocation;

@end

@implementation SNWResortCollectionView

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.theme = [SNWTheme themeFromPlistNamed:@"Styles"];
    self.collectionView.backgroundColor = self.theme.baseBackground;

    self.getUserLocation = [SNWGetLocation new];
    [self.getUserLocation findCurrentLocation];

    [self styleNavigationBar];
    [self initNibForCollectionView];

    self.getResorts = [SNWService new];

    [[[RACObserve(self.getUserLocation, currentLocation) ignore:nil]
     flattenMap:^id(CLLocation *location) {
         return [self.getResorts fetchResortsNearWithUserLocation:location.coordinate];
     }]
     subscribeNext:^(NSArray *resorts) {
         self.resorts = resorts;
        [self.collectionView reloadData];
     }];
}

- (void)initNibForCollectionView
{
    UINib *cellNib = [UINib nibWithNibName:@"SNWResortCellNib" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return (NSInteger)[self.resorts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SNWResortCell *cell = (SNWResortCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    SNWResortPreview *resortForCell = [self.resorts objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor colorWithRed:26.0f/255.0f green:27.0f/255.0f blue:34.0f/255.0f alpha:1.0];
    cell.resortNameLabel.textColor = [UIColor whiteColor];
    cell.resortNameLabel.font = [UIFont fontWithName:@"DINAlternate-Bold" size:14.0];
    cell.resortNameLabel.text = resortForCell.name;

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"showResortDetail" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showResortDetail"]) {
        SNWResortDetailViewController *detailViewController = [segue destinationViewController];
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
        SNWResortPreview *resortForDetail = [self.resorts objectAtIndex:indexPath.row];
        detailViewController.resort = resortForDetail;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height / 6);
}


- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(8, 16, 0, 16);
}

- (IBAction)changeCollectionViewData:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex)
    {
        case 0:
            self.resorts = [self.getResorts alpineResorts];
            break;
        case 1:
            self.resorts = [self.getResorts nordicResorts];
            break;
    }

    [self.collectionView reloadData];
}

- (void)styleNavigationBar
{
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];

    self.segmentedControl.frame = CGRectMake(0, 0, 140, 26);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:nil
                                                                            action:nil];

    self.navigationController.navigationBar.tintColor = self.theme.slate;
    self.navigationController.navigationBar.backgroundColor = self.theme.baseBackground;
    self.navigationController.navigationBar.barTintColor = self.theme.baseBackground;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}
@end
