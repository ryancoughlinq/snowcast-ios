#import "SNWCircularProgressLayer.h"

CGFloat degreesToRadians(CGFloat degrees) {
    return degrees * (M_PI / 180.0f);
}

@implementation SNWCircularProgressLayer

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.lineWidth = 2.0f;
    self.strokeColor = [[UIColor greenColor] CGColor];
    self.lineCap = kCALineCapRound;
    self.fillColor = [[UIColor clearColor] CGColor];
    self.backgroundColor = [[UIColor clearColor] CGColor];
    self.strokeEnd = 0.0f;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:nil];
    animation.duration = 2.5;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];

    self.actions = @{@"strokeEnd": animation};
    
    return self;
}

- (void)setProgress:(CGFloat)progress
{
    if (progress == _progress) return;
    _progress = progress;
    self.strokeEnd = _progress;
}

- (void)layoutSublayers
{
    [super layoutSublayers];
    CGPoint center = CGPointMake(CGRectGetWidth(self.bounds) / 2.0f, CGRectGetHeight(self.bounds) / 2.0f);
    CGFloat radius = floor(MIN(center.x, center.y) - (self.lineWidth / 2.0f));
    CGFloat startAngle = degreesToRadians(-90.0f);
    CGFloat endAngle = degreesToRadians(270.0f);
    self.path = [[UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES] CGPath];
}

@end
