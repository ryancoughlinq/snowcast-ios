@interface SNWResortPieView : UIView

@property (weak, nonatomic) IBOutlet UILabel *pieLabel;
@property (weak, nonatomic) IBOutlet UILabel *pieValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pieImageView;

@property (nonatomic) CGFloat progress;
@property (nonatomic) IBInspectable UIColor *color;
@property (nonatomic) IBInspectable UIImage *pieImage;
@property (nonatomic) IBInspectable BOOL isIconHidden;
@property (nonatomic) IBInspectable BOOL isLabelHidden;

@end
