#import <Mantle.h>

@interface SNWWeather : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *precipType;
@property (nonatomic, strong) NSNumber *precipProbability;
@property (nonatomic, strong) NSNumber *temperatureMin;
@property (nonatomic, strong) NSNumber *temperatureMax;
@property (nonatomic, strong) NSNumber *windSpeed;
@property (nonatomic, strong) NSDate *date;

@end
