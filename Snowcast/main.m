 //
//  main.m
//  Snowcast
//
//  Created by Ryan Coughlin on 6/26/14.
//  Copyright (c) 2014 Ryan Coughlin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SNWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SNWAppDelegate class]));
    }
}
