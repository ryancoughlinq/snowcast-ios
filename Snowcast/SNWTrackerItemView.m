//
//  SNWTrackerDetailView.m
//  Pods
//
//  Created by Ryan Coughlin on 12/21/14.
//
//

#import "SNWTrackerItemView.h"

@implementation SNWTrackerItemView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.subviews.count == 0) {
            UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
            UIView *subview = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
            [self addSubview:subview];
            self.clipsToBounds = YES;
            [self layoutSubviews];
        }
    }

    return self;
}


@end
