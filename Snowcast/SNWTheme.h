#import "TRIMTheme.h"

@interface SNWTheme : TRIMTheme

@property (nonatomic, readonly) UIColor *baseBackground;
@property (nonatomic, readonly) UIColor *mainBlue;
@property (nonatomic, readonly) UIColor *iceGreen;
@property (nonatomic, readonly) UIColor *slate;
@property (nonatomic, readonly) UIColor *lightGrey;
@property (nonatomic, readonly) UIColor *collectionItemBackground;
@property (nonatomic, readonly) UIColor *lightGreen;
@property (nonatomic, readonly) UIColor *green;
@property (nonatomic, readonly) UIColor *navBarTintColor;
@property (nonatomic, readonly) UIColor *lightPurple;

@end
