@import CoreLocation;

@interface SNWTrackLocation : NSObject

@property (nonatomic, strong) CLLocation *currentLocation;

- (void)beginTrackingUserMovements;
- (void)stopTrackingUserMovements;

@end
