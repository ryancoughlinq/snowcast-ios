#import "SNWGetLocation.h"

@interface SNWResortCollectionView : UIViewController

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *openTracker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end
