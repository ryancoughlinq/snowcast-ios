#import "SNWLocationUpdate.h"
#import "SNWRunData.h"
#import "SNWTrackLocation.h"
#import "SNWRunData.h"
#import "SNWTrackerController.h"

@interface SNWTrackerController ()

@property (nonatomic, strong) SNWTrackLocation *locationManager;
@property (nonatomic) NSMutableArray *rawLocationUpdates;
@property (nonatomic) NSDate *startDate;
@property (nonatomic) NSTimeInterval elapsedTime;

@end

@implementation SNWTrackerController

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;

    self.rawLocationUpdates = [[NSMutableArray alloc] init];
    self.locationManager = [SNWTrackLocation new];

    [[RACObserve(self.locationManager, currentLocation) ignore:nil]
     subscribeNext:^(CLLocation *location) {
         [self buildRunWithLocation:location];
     }];

    return self;
}

- (RACSignal *)timerWithPauseButton:(RACSignal *)pauseButton
{
    [self.locationManager beginTrackingUserMovements];
    self.startDate = [NSDate date];
    NSLog(@"startDate: %@", self.startDate);
    NSLog(@"self.elapsedTime: %f", self.elapsedTime);

    return [[[[[RACSignal interval:(1.0 / 60.0) onScheduler:[RACScheduler schedulerWithPriority:RACSchedulerPriorityHigh]] startWith:self.startDate]
              takeUntil:pauseButton] map:^id(id x) {
              self.elapsedTime = [x timeIntervalSinceDate:self.startDate];
        return [self formatStringWithInterval:self.elapsedTime];
    }] deliverOnMainThread];
}

- (NSString *)formatStringWithInterval:(NSTimeInterval)timeInterval
{
    int minutes = (int)(timeInterval / 60.0);
    int seconds = (int)(timeInterval = timeInterval - (minutes * 60));
    int milliseconds = (int)(timeInterval * 10);
    return [NSString stringWithFormat:@"%02u:%02u:%02u", minutes, seconds, milliseconds];
}

- (RACSignal *)buildRun
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        SNWRunData *runData = [[SNWRunData alloc] initWithArray:self.rawLocationUpdates];
        [self sendDataToRealmWithRunData:runData];
        [subscriber sendNext:runData];
        return nil;
    }];
}

- (RACSignal *)createViewModelWithRunData:(SNWRunData *)runData
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        SNWTrackerViewModel *trackerViewModel = [[SNWTrackerViewModel alloc] initWithRunData:runData];
        [subscriber sendNext:trackerViewModel];
        return nil;
    }];
}

- (void)sendDataToRealmWithRunData:(SNWRunData *)runData
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:runData];
    [realm commitWriteTransaction];
}

- (void)buildRunWithLocation:(CLLocation *)location
{
    SNWLocationUpdate *locationUpdate = [[SNWLocationUpdate alloc] initWithLocation:location];
    [self.rawLocationUpdates addObject:locationUpdate];
}

@end
