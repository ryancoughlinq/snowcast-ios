#import <QuartzCore/QuartzCore.h>

@interface SNWCircularProgressLayer : CAShapeLayer

@property (nonatomic) CGFloat progress;

@end
