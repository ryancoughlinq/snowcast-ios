#import "SNWTabBarController.h"

@interface SNWTabBarController ()

@end

@implementation SNWTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITabBar *tabBar = self.tabBar;
    UITabBarItem *conditions = [tabBar.items objectAtIndex:0];
    UITabBarItem *tracker = [tabBar.items objectAtIndex:1];

    conditions.image = [UIImage imageNamed:@"conditions-unselected"];
    conditions.selectedImage = [UIImage imageNamed:@"conditions-selected"];

    tracker.image = [UIImage imageNamed:@"tracker-unselected"];
    tracker.selectedImage = [UIImage imageNamed:@"tracker-selected"];
}

@end
