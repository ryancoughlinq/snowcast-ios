@import CoreLocation;

#import "SNWService.h"
#import "SNWResortPreview.h"
#import "SNWResort.h"
#import "SNWWeather.h"
#import "SNWGetLocation.h"

#import <BlocksKit.h>
#import <SafeCast.h>
#import <Mantle.h>

static NSString* const kForecastIOAPIKey = @"5ad759e29656a49d5b019059b3233cc9";
static NSString* const kFetchBaseURL = @"http://localhost:9999/api/";
static NSString* const kFetchSingleResort = @"get-resort-info/%@";
static NSString* const kFetchWeather = @"https://api.forecast.io/forecast/%@/%@";

static NSString* const SNWAlpineKey = @"NA_Alpine";
static NSString* const SNWNordicKey = @"XC";

@implementation SNWService

- (instancetype)init
{
    if (self = [super init]) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        _session = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}

- (RACSignal *)fetchDataWithURL: (NSURL *)url
{
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURLSessionDataTask *dataTask = [self.session dataTaskWithURL:url
                                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                         if(!error) {
                                                             id json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                              options:kNilOptions
                                                                                                         error:&error];
                                                             if(json) {
                                                                 [subscriber sendNext:json];
                                                             }
                                                         } else {
                                                             [subscriber sendError:error];
                                                         }
                                                         [subscriber sendCompleted];
                                                     }];
        [dataTask resume];

        return [RACDisposable disposableWithBlock:^{
            [dataTask cancel];
        }];
    }]
    deliverOn:RACScheduler.mainThreadScheduler];
}

- (RACSignal *)fetchWeatherWithLocation:(CLLocationCoordinate2D)coordinate
{
    NSString *weatherURLString = [NSString stringWithFormat:kFetchWeather, kForecastIOAPIKey, [self formatLonLatAsComma:coordinate]];
    return [[[self fetchDataWithURL:[self createURLWithString:weatherURLString]] map:^(NSDictionary *weatherData) {
        return [self buildDailyWeatherInformationWithDictionary:weatherData];
    }]
    deliverOn:RACScheduler.mainThreadScheduler];
    
}

- (RACSignal *)fetchAllResorts
{
    return [[[self fetchDataWithURL:[self createURLWithParameters:@"all_resorts"]] map:^(id resorts) {
        return [MTLJSONAdapter modelsOfClass:[SNWResortPreview class] fromJSONArray:resorts[@"data"] error:nil];
    }]
    deliverOn:RACScheduler.mainThreadScheduler];

}

- (RACSignal *)fetchResortWithID:(NSNumber *)resortID
{
    NSString *resortStringForURL = [NSString stringWithFormat:kFetchSingleResort, resortID];
    return [[[self fetchDataWithURL:[self createURLWithParameters:resortStringForURL]] map:^(NSDictionary *resort) {
        NSDictionary *singleResort = [NSDictionary safe_cast:resort[@"items"][0]];
        return [[SNWResort alloc] initWithResortDictionary:singleResort];
    }]
    deliverOn:RACScheduler.mainThreadScheduler];
}

- (RACSignal *)fetchResortsNearWithUserLocation:(CLLocationCoordinate2D)coordinate
{
    NSString *resortStringForURL = [NSString
                                    stringWithFormat:@"near/%@/4/10", [self convertCoordinateToString:coordinate]];

    return [[[self fetchDataWithURL:[self createURLWithParameters:resortStringForURL]] map:^(id resorts) {

        NSError *modelError = nil;

        NSArray *unfilteredResorts = [MTLJSONAdapter modelsOfClass:[SNWResortPreview class] fromJSONArray:resorts[@"data"] error:&modelError];

        if(modelError) {
            NSLog(@"Error: %@", modelError);
        }

        return [self setFilteredResortsForTypeWithArray:unfilteredResorts];
    }]
    deliverOn:RACScheduler.mainThreadScheduler];
}

- (NSDictionary *)setFilteredResortsForTypeWithArray:(NSArray *)unfilteredResorts
{
    self.alpineResorts = [[NSMutableArray alloc] init];
    self.nordicResorts = [[NSMutableArray alloc] init];

    NSDictionary *resortArrayForKey = @{
                                        SNWAlpineKey : self.alpineResorts,
                                        SNWNordicKey : self.nordicResorts
                                        };

    [unfilteredResorts enumerateObjectsUsingBlock:^(SNWResortPreview *resort, NSUInteger idx, BOOL *stop) {
        [resortArrayForKey[resort.type] addObject:resort];
    }];

    return resortArrayForKey[SNWAlpineKey];
}

- (NSArray *)buildDailyWeatherInformationWithDictionary:(NSDictionary *)dictionary
{
    NSMutableArray *weatherObjects = [[NSMutableArray alloc] init];

    [dictionary[@"daily"][@"data"] bk_each:^(NSDictionary *data) {
        NSError *error = nil;
        SNWWeather *weather = [MTLJSONAdapter modelOfClass:[SNWWeather class] fromJSONDictionary:data error:nil];
        if (!error) {
            [weatherObjects addObject:weather];
        } else {
            NSLog(@"Error: %@", error);
        }
    }];

    return (NSArray *)weatherObjects;
}

- (NSString *)convertCoordinateToString:(CLLocationCoordinate2D)coordinate
{
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *formattedString = [NSString stringWithFormat:@"%@/%@", longitude, latitude];

    return formattedString;
}
            
- (NSURL *)createURLWithString:(NSString *)string
{
    NSString *stringForURL = [NSString stringWithFormat:@"%@", string];
    return [NSURL URLWithString:stringForURL];
}

- (NSURL *)createURLWithParameters:(NSString *)resortParameters
{
    NSString *stringForURL = [NSString stringWithFormat:@"%@%@", kFetchBaseURL, resortParameters];
    return [NSURL URLWithString:stringForURL];
}
            
- (NSString *)formatLonLatAsComma:(CLLocationCoordinate2D)coordinate
{
    NSString *formattedLonLat = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
    return formattedLonLat;
}
            

@end
