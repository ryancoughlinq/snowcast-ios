#import "SNWWeather.h"

@implementation SNWWeather : MTLModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"summary": @"summary",
             @"date": @"time",
             @"icon": @"icon",
             @"precipProbability": @"precipProbability",
             @"precipType": @"precipType",
             @"temperatureMin": @"temperatureMin",
             @"temperatureMax": @"temperatureMax",
             @"windSpeed": @"windSpeed"
             };
}

+ (NSValueTransformer *)dateJSONTransformer {
    return [MTLValueTransformer transformerWithBlock:^id(id value) {
        NSTimeInterval seconds = [value doubleValue];
        return [NSDate dateWithTimeIntervalSince1970:seconds];
    }];
}

@end
