@interface SNWTrackerRunTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *runDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxSpeed;

@end
