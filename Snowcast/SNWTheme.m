#import "SNWTheme.h"

@implementation SNWTheme

@dynamic baseBackground, iceGreen, mainBlue, slate, lightGrey, collectionItemBackground, green, lightGreen, navBarTintColor, lightPurple;

@end
