#import "SNWTrackPoint.h"

@implementation SNWTrackPoint

- (id)initWithCoordinate:(CLLocationCoordinate2D)newCoordinate {
    
    self = [super init];
    if (self) {
        _coordinate = newCoordinate;
    }
    return self;
}

@end
