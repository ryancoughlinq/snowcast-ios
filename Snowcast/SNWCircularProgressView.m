#import "SNWCircularProgressView.h"
#import "SNWCircularProgressLayer.h"

@interface SNWCircularProgressView ()

@property (nonatomic) SNWCircularProgressLayer *progressLayer;

@end

@implementation SNWCircularProgressView

- (SNWCircularProgressLayer *)progressLayer
{
    if (!_progressLayer) {
        _progressLayer = [SNWCircularProgressLayer layer];
        [self.layer addSublayer:_progressLayer];
    }
    
    return _progressLayer;
}

- (void)setProgress:(CGFloat)progress
{
    if (_progress == progress) return;
    _progress = progress;
    self.progressLayer.progress = _progress;
}

- (void)setProgressColor:(UIColor *)progressColor
{
    self.progressLayer.strokeColor = progressColor.CGColor;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.progressLayer.frame = self.bounds;
}

@end
