//
//  SNWTrackerRunStatsView.m
//  Snowcast
//
//  Created by Ryan Coughlin on 12/22/14.
//  Copyright (c) 2014 Ryan Coughlin. All rights reserved.
//

@import QuartzCore;
#import "SNWTheme.h"
#import "SNWTrackerRunStatsView.h"

const float kBorderThickness = 0.50f;
const float kCellHeight = 66.0f;

@interface SNWTrackerRunStatsView ()

@property (nonatomic, strong) SNWTheme *theme;

@end

@implementation SNWTrackerRunStatsView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.theme = [SNWTheme themeFromPlistNamed:@"Styles"];
    }

    return self;
}

- (void)drawRect:(CGRect)rect
{
    [self drawRightBorder];
    [self drawTopBorderWithYPos:0];
    [self drawTopBorderWithYPos:kCellHeight];
    [self drawTopBorderWithYPos:(kCellHeight * 2) - 2.0f];
}

- (void)drawTopBorderWithYPos:(float)yPos
{
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0, yPos, self.frame.size.width, kBorderThickness);
    bottomBorder.backgroundColor = self.theme.slate.CGColor;
    [self.layer addSublayer:bottomBorder];
}

- (void)drawRightBorder
{
    CALayer *rightBorder = [CALayer layer];
    rightBorder.frame = CGRectMake(self.frame.size.width / 2, 0, kBorderThickness, kCellHeight * 2);
    rightBorder.backgroundColor = self.theme.slate.CGColor;
    [self.layer addSublayer:rightBorder];
}

@end
