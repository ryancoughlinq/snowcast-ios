#import "SNWTrackerMapView.h"
#import "SNWTrackerController.h"
#import "SNWLocationUpdate.h"
#import <BlocksKit.h>

@implementation SNWTrackerMapView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        self.delegate = self;
        self.mapType = MKMapTypeSatellite;
    }
    return self;
}

- (void)drawPolylineWithLocationArray:(RLMArray *)array
{
    NSMutableArray *locationObjects = [[NSMutableArray alloc] init];

    for (int i = 0; i < [array count]; i++) {

        SNWLocationUpdate *locationUpdate = [array objectAtIndex:i];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(locationUpdate.latitude, locationUpdate.longitude);
        CLLocation *location = [[CLLocation alloc] initWithCoordinate:coordinate
                                                             altitude:locationUpdate.altitude
                                                   horizontalAccuracy:locationUpdate.horizontalAccuracy
                                                     verticalAccuracy:locationUpdate.verticalAccuracy
                                                               course:locationUpdate.course
                                                                speed:locationUpdate.speed
                                                            timestamp:locationUpdate.timestamp];

        [locationObjects addObject:location];
    };

    CLLocationCoordinate2D coordinates[locationObjects.count];
    int i = 0;
    for (CLLocation *location in locationObjects) {
        coordinates[i] = location.coordinate;
        i++;
    }

    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coordinates count:array.count];
    [self addOverlay:polyline level:MKOverlayLevelAboveRoads];
    [self zoomToPolyLine:self polyLine:polyline animated:NO];
}

- (void)zoomToPolyLine:(MKMapView *)map
              polyLine:(MKPolyline *)polyLine
              animated:(BOOL)animated
{
    [map setVisibleMapRect:[polyLine boundingMapRect] edgePadding:UIEdgeInsetsMake(15.0,15.0,15.0,15.0) animated:animated];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView
            rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        renderer.strokeColor = [[UIColor cyanColor] colorWithAlphaComponent:1.0];
        renderer.lineWidth = 4;

        return renderer;
    }
    
    return nil;
}

@end
