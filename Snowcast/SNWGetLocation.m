#import "SNWGetLocation.h"

@interface SNWGetLocation ()

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation SNWGetLocation

- (id)init
{
    self = [super init];

    if (self == nil) {
        return nil;
    }

    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;

    return self;
}

- (void)findCurrentLocation
{
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }

    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.locationManager.distanceFilter = 2000;
    
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    self.currentLocation = location;
    NSLog(@"Location: %@", location);
}

@end
