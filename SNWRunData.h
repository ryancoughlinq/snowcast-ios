#import <Realm/Realm.h>
#import "SNWLocationUpdate.h"

@interface SNWRunData : RLMObject

- (id)initWithArray:(NSArray *)array;

@property double maxSpeed;
@property double maxAltitude;
@property double altitudeDelta;
@property double averageSpeed;
@property double distance;
@property NSDate *date;
@property RLMArray <SNWLocationUpdate> *locationUpdates;

@end
