#import "SNWLocationUpdate.h"

@interface SNWLocationUpdate ()

@property (nonatomic, readonly) CLLocation *location;

@end

@implementation SNWLocationUpdate

@synthesize location = _location;

- (id)initWithLocation:(CLLocation *)location
{
    self = [super init];

    if (self) {
        _location = location;
        self.altitude = _location.altitude;
        self.speed = _location.speed;
        self.course = _location.course;
        self.timestamp = _location.timestamp;
        self.longitude = _location.coordinate.longitude;
        self.latitude = _location.coordinate.latitude;
        self.verticalAccuracy = _location.verticalAccuracy;
        self.horizontalAccuracy = _location.horizontalAccuracy;
    }
    
    return self;
}

- (NSArray *)trackRunOwner {
    return [self linkingObjectsOfClass:@"SNWRunData" forProperty:@"locationUpdates"];
}

@end
