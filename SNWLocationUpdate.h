@import MapKit;
#import <Realm.h>

@interface SNWLocationUpdate : RLMObject

@property double speed;
@property double course;
@property double altitude;
@property double latitude;
@property double longitude;
@property double verticalAccuracy;
@property double horizontalAccuracy;
@property NSDate *timestamp;

- (id)initWithLocation:(CLLocation *)location;
- (NSArray *)trackRunOwner;

@end

RLM_ARRAY_TYPE(SNWLocationUpdate);
