#import "SNWRunData.h"
#import "SNWTrackerMapView.h"
#import "SNWLocationUpdate.h"
#import <BlocksKit.h>

const NSString *kMPHDefault = @"-- mph";
const NSString *kDistanceDefault = @"-- ft";

@interface SNWRunData ()

@property (nonatomic, readonly) NSArray *locationArray;

@end

@implementation SNWRunData

@synthesize locationArray = _locationArray;

- (instancetype)initWithArray:(NSArray *)array
{
    self = [super init];

    if(self == nil) {
        return nil;
    }

    _locationArray = [array bk_map:^id(SNWLocationUpdate *locationUpdate) {

        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(locationUpdate.latitude, locationUpdate.longitude);
        CLLocation *location = [[CLLocation alloc] initWithCoordinate:coordinate
                                                             altitude:locationUpdate.altitude
                                                   horizontalAccuracy:0
                                                     verticalAccuracy:0
                                                               course:locationUpdate.course
                                                                speed:locationUpdate.speed
                                                            timestamp:locationUpdate.timestamp];
        [self.locationUpdates addObject:locationUpdate];

        return location;
    }];

    _date = [NSDate date];
    _distance = [self findTotalDistanceWithArray:_locationArray];
    _averageSpeed = [self findAverageSpeedWithArray:_locationArray];
    _maxSpeed = [self findMaxSpeedWithArray:_locationArray];
    _altitudeDelta = [self findAltitudeDeltaWithArray:_locationArray];
    _maxAltitude = [self findMaxAltitudeWithArray:_locationArray];

    return self;
}

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"maxSpeed" : kMPHDefault,
             @"maxAltitude" : kDistanceDefault,
             @"altitudeDelta" : kDistanceDefault,
             @"averageSpeed" : kMPHDefault,
             @"distance" : kDistanceDefault};
}


- (double)findTotalDistanceWithArray:(NSArray *)array
{
    double totalDistance = 0;
    CLLocation *firstLocation = array.firstObject;

    for (NSUInteger i = 1; i < [array count]; i++) {
        CLLocation *locDistance = array[i];
        totalDistance += [firstLocation distanceFromLocation:locDistance];
        firstLocation = locDistance;
    }

    return totalDistance;
}

- (double)findAverageSpeedWithArray:(NSArray *)array
{
    return [[array valueForKeyPath:@"@avg.speed"] doubleValue];

}

- (double)findMaxSpeedWithArray:(NSArray *)array
{
    return [[array valueForKeyPath:@"@max.speed"] doubleValue];
}

- (double)findMaxAltitudeWithArray:(NSArray *)array
{
    return [[array valueForKeyPath:@"@max.altitude"] doubleValue];
}

- (double)findAltitudeDeltaWithArray:(NSArray *)array
{
    double maxAltitude = [[array valueForKeyPath:@"@max.altitude"] doubleValue];
    double minAltitude = [[array valueForKeyPath:@"@min.altitude"] doubleValue];
    return maxAltitude - minAltitude;
}

@end
