//
//  SnowcastTests.m
//  SnowcastTests
//
//  Created by Ryan Coughlin on 6/26/14.
//  Copyright (c) 2014 Ryan Coughlin. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SnowcastTests : XCTestCase

@end

@implementation SnowcastTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
